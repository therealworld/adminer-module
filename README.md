# Update Adminer

## Download

* go to ["Adminer"](https://www.adminer.org/de/#download)
* download the File Adminer X.X.X for MySQL
* save it to .\Adminer\
* change in the last line of **36ba9de7be2f8a24dc54424f309d8bc0.php**
  * find the old included adminer
  * and change it to the new version

## Update CSS

* I use the CSS from ["Klemens Häckel"](https://raw.githubusercontent.com/vrana/adminer/master/designs/haeckel/adminer.css)
* I´ve downloaded and add some stuff to hide the database-selection and language-section. Both is not necessary for OXID
* see resouces\adminer.css
* Then I´ve compressed the file and put it in the root