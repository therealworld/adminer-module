<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use TheRealWorld\AdminerModule\Application\Controller\Admin\AdminerController;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwadminer',
    'title' => [
        'de' => 'the-real-world - Adminer für OXID',
        'en' => 'the-real-world - Adminer for OXID',
    ],
    'description' => [
        'de' => 'Integration des Datenbank-Tools Adminer (https://www.adminer.org/) in das OXID-Backend',
        'en' => 'Integration of the database tool Adminer (https://www.adminer.org/) into the OXID backend',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwadminer'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\AdminerModule\Core\AdminerEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\AdminerModule\Core\AdminerEvents::onDeactivate',
    ],
    'controllers' => [
        'AdminerController' => AdminerController::class,
    ],
];
