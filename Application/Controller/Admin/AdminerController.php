<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\AdminerModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Registry;

/**
 * Class Main.
 */
class AdminerController extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $oConfig = Registry::getConfig();

        $sPort = $oConfig->getConfigParam('dbPort');
        $sHost = $oConfig->getConfigParam('dbHost');
        $sHost .= $sPort ? ':' . $sPort : '';
        $sUser = $oConfig->getConfigParam('dbUser');
        $sDB = $oConfig->getConfigParam('dbName');

        $aGet = [
            'server'   => $sHost,
            'username' => $sUser,
            'db'       => $sDB,
        ];

        // Shop Module Path
        $oModule = oxNew(Module::class);
        $sRelativePath = $oConfig->getModulesDir(false) . $oModule->getModulePath('trwadminer');
        $sRelativePath .= '/Adminer/';
        $sPath = $oConfig->getConfigParam('sShopDir');
        $sAdminerPath = $sPath . $sRelativePath;
        $sHtaccessPath = $sAdminerPath . '.htaccess';

        // find special file
        $pattern = '/^(?!adminer).+\.php$/';
        $sFile = '';
        $aAdminerFiles = scandir($sAdminerPath);
        foreach ($aAdminerFiles as $sAdminerFile) {
            if (preg_match($pattern, $sAdminerFile)) {
                $sFile = $sAdminerFile;

                break;
            }
        }

        // check if htaccess exists
        if (!file_exists($sHtaccessPath)) {
            // randomize the adminer-file
            $sNewFile = Registry::getUtilsObject()->generateUID() . '.php';
            [$sOldFile, $sFile] = [$sFile, $sNewFile];
            rename($sAdminerPath . $sOldFile, $sAdminerPath . $sFile);

            $data = fopen($sHtaccessPath, 'wb');
            $sEscapedFile = str_replace('.', '\.', $sFile);
            fwrite($data, sprintf(
                'Order Allow,Deny' . "\n" .
                '<FilesMatch "(%s|adminer\.css)">' . "\n" .
                'Allow from all' . "\n" .
                '</FilesMatch>' . "\n",
                $sEscapedFile
            ));
            fclose($data);
        }

        $sUrl = $oConfig->getShopMainUrl() . $sRelativePath . $sFile . '?' . http_build_query($aGet);

        Registry::getUtils()->redirect($sUrl, false, 303);

        return '';
    }
}
