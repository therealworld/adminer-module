<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

use plugins\AdminerDatabaseHide;
use plugins\AdminerFrames;
use plugins\AdminerImportPath;
use plugins\AdminerPlugin;

define('DS', constant('DIRECTORY_SEPARATOR'));

$root = __DIR__ . DS . '..' . DS . '..' . DS . '..' . DS . '..' . DS;

function adminer_object() {
    global $root, $sDb;

    // required to run any plugin
    include_once '.' . DS . 'plugins' . DS . 'plugin.php';

    include_once '.' . DS . 'plugins' . DS . 'adminerframes.php';
    include_once '.' . DS . 'plugins' . DS . 'adminerdatabasehide.php';
    include_once '.' . DS . 'plugins' . DS . 'adminerimportpath.php';
    $sDb = $_GET['db'] ?? '';

    $plugins = [
        new AdminerFrames(true),
        new AdminerDatabaseHide($sDb),
        new AdminerImportPath($root . 'import'. DS)
    ];
    return new AdminerPlugin($plugins);
}

include '.' . DS . 'adminer-de.php';