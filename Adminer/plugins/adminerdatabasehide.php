<?php

namespace plugins;
/** Hide some databases from the interface - just to improve design, not a security plugin
 * @link https://www.adminer.org/plugins/#use
 * @author Jakub Vrana, https://www.vrana.cz/
 * @license https://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */
class AdminerDatabaseHide
{

    protected string $allowed;

    public function __construct(string $allowed)
    {
        $this->allowed = strtolower($allowed);
    }

    function databases($flush = true)
    {
        $return = [];
        foreach (get_databases($flush) as $db) {
            if (strtolower($db) === $this->allowed) {
                $return[] = $db;
            }
        }
        return $return;
    }


}
