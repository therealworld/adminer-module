<?php

namespace plugins;
/** Hide some databases from the interface - just to improve design, not a security plugin
 * @link https://www.adminer.org/plugins/#use
 * @author Jakub Vrana, https://www.vrana.cz/
 * @license https://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */
class AdminerImportPath
{

    protected string $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /** Set the path of the file for webserver load
     * @return string path of the sql dump file
     */
    function importServerPath()
    {
        return realpath($this->path) . DIRECTORY_SEPARATOR . "adminer.sql";
    }
}
